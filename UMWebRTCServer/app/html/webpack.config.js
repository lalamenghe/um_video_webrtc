const path = require('path')

let config = {
    mode: "production",
    entry: {
        umroom: path.join(__dirname, './js/index.js'),
        camera_room: path.join(__dirname, './js/camera.js'),
        camera_list_room: path.join(__dirname, './js/camera_list.js'),
        camera_view_room: path.join(__dirname, './js/camera_view.js'),
        group_call_room: path.join(__dirname, "./js/group_call.js")
    },
    output: {
        filename: '[name].js',
        path: path.join(__dirname, './dist'),
        library: "UmRoom",
        libraryTarget: "var"
    }
}

module.exports = config