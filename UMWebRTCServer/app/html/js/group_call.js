// md5
/*
 * A JavaScript implementation of the RSA Data Security, Inc. MD5 Message
 * Digest Algorithm, as defined in RFC 1321.
 * Version 2.1 Copyright (C) Paul Johnston 1999 - 2002.
 * Other contributors: Greg Holt, Andrew Kepert, Ydnar, Lostinet
 * Distributed under the BSD License
 * See http://pajhome.org.uk/crypt/md5 for more info.
 */
 
/*
 * Configurable variables. You may need to tweak these to be compatible with
 * the server-side, but the defaults work in most cases.
 */
var hexcase = 0;  /* hex output format. 0 - lowercase; 1 - uppercase        */
var b64pad  = ""; /* base-64 pad character. "=" for strict RFC compliance   */
var chrsz   = 8;  /* bits per input character. 8 - ASCII; 16 - Unicode      */

/*
 * These are the functions you'll usually want to call
 * They take string arguments and return either hex or base-64 encoded strings
 */
function hex_md5(s){ return binl2hex(core_md5(str2binl(s), s.length * chrsz));}
function b64_md5(s){ return binl2b64(core_md5(str2binl(s), s.length * chrsz));}
function str_md5(s){ return binl2str(core_md5(str2binl(s), s.length * chrsz));}
function hex_hmac_md5(key, data) { return binl2hex(core_hmac_md5(key, data)); }
function b64_hmac_md5(key, data) { return binl2b64(core_hmac_md5(key, data)); }
function str_hmac_md5(key, data) { return binl2str(core_hmac_md5(key, data)); }
 
/*
 * Perform a simple self-test to see if the VM is working
 */
function md5_vm_test()
{
  return hex_md5("abc") == "900150983cd24fb0d6963f7d28e17f72";
}
 
/*
 * Calculate the MD5 of an array of little-endian words, and a bit length
 */
function core_md5(x, len)
{
  /* append padding */
  x[len >> 5] |= 0x80 << ((len) % 32);
  x[(((len + 64) >>> 9) << 4) + 14] = len;
 
  var a =  1732584193;
  var b = -271733879;
  var c = -1732584194;
  var d =  271733878;
 
  for(var i = 0; i < x.length; i += 16)
  {
    var olda = a;
    var oldb = b;
    var oldc = c;
    var oldd = d;
 
    a = md5_ff(a, b, c, d, x[i+ 0], 7 , -680876936);
    d = md5_ff(d, a, b, c, x[i+ 1], 12, -389564586);
    c = md5_ff(c, d, a, b, x[i+ 2], 17,  606105819);
    b = md5_ff(b, c, d, a, x[i+ 3], 22, -1044525330);
    a = md5_ff(a, b, c, d, x[i+ 4], 7 , -176418897);
    d = md5_ff(d, a, b, c, x[i+ 5], 12,  1200080426);
    c = md5_ff(c, d, a, b, x[i+ 6], 17, -1473231341);
    b = md5_ff(b, c, d, a, x[i+ 7], 22, -45705983);
    a = md5_ff(a, b, c, d, x[i+ 8], 7 ,  1770035416);
    d = md5_ff(d, a, b, c, x[i+ 9], 12, -1958414417);
    c = md5_ff(c, d, a, b, x[i+10], 17, -42063);
    b = md5_ff(b, c, d, a, x[i+11], 22, -1990404162);
    a = md5_ff(a, b, c, d, x[i+12], 7 ,  1804603682);
    d = md5_ff(d, a, b, c, x[i+13], 12, -40341101);
    c = md5_ff(c, d, a, b, x[i+14], 17, -1502002290);
    b = md5_ff(b, c, d, a, x[i+15], 22,  1236535329);
 
    a = md5_gg(a, b, c, d, x[i+ 1], 5 , -165796510);
    d = md5_gg(d, a, b, c, x[i+ 6], 9 , -1069501632);
    c = md5_gg(c, d, a, b, x[i+11], 14,  643717713);
    b = md5_gg(b, c, d, a, x[i+ 0], 20, -373897302);
    a = md5_gg(a, b, c, d, x[i+ 5], 5 , -701558691);
    d = md5_gg(d, a, b, c, x[i+10], 9 ,  38016083);
    c = md5_gg(c, d, a, b, x[i+15], 14, -660478335);
    b = md5_gg(b, c, d, a, x[i+ 4], 20, -405537848);
    a = md5_gg(a, b, c, d, x[i+ 9], 5 ,  568446438);
    d = md5_gg(d, a, b, c, x[i+14], 9 , -1019803690);
    c = md5_gg(c, d, a, b, x[i+ 3], 14, -187363961);
    b = md5_gg(b, c, d, a, x[i+ 8], 20,  1163531501);
    a = md5_gg(a, b, c, d, x[i+13], 5 , -1444681467);
    d = md5_gg(d, a, b, c, x[i+ 2], 9 , -51403784);
    c = md5_gg(c, d, a, b, x[i+ 7], 14,  1735328473);
    b = md5_gg(b, c, d, a, x[i+12], 20, -1926607734);
 
    a = md5_hh(a, b, c, d, x[i+ 5], 4 , -378558);
    d = md5_hh(d, a, b, c, x[i+ 8], 11, -2022574463);
    c = md5_hh(c, d, a, b, x[i+11], 16,  1839030562);
    b = md5_hh(b, c, d, a, x[i+14], 23, -35309556);
    a = md5_hh(a, b, c, d, x[i+ 1], 4 , -1530992060);
    d = md5_hh(d, a, b, c, x[i+ 4], 11,  1272893353);
    c = md5_hh(c, d, a, b, x[i+ 7], 16, -155497632);
    b = md5_hh(b, c, d, a, x[i+10], 23, -1094730640);
    a = md5_hh(a, b, c, d, x[i+13], 4 ,  681279174);
    d = md5_hh(d, a, b, c, x[i+ 0], 11, -358537222);
    c = md5_hh(c, d, a, b, x[i+ 3], 16, -722521979);
    b = md5_hh(b, c, d, a, x[i+ 6], 23,  76029189);
    a = md5_hh(a, b, c, d, x[i+ 9], 4 , -640364487);
    d = md5_hh(d, a, b, c, x[i+12], 11, -421815835);
    c = md5_hh(c, d, a, b, x[i+15], 16,  530742520);
    b = md5_hh(b, c, d, a, x[i+ 2], 23, -995338651);
 
    a = md5_ii(a, b, c, d, x[i+ 0], 6 , -198630844);
    d = md5_ii(d, a, b, c, x[i+ 7], 10,  1126891415);
    c = md5_ii(c, d, a, b, x[i+14], 15, -1416354905);
    b = md5_ii(b, c, d, a, x[i+ 5], 21, -57434055);
    a = md5_ii(a, b, c, d, x[i+12], 6 ,  1700485571);
    d = md5_ii(d, a, b, c, x[i+ 3], 10, -1894986606);
    c = md5_ii(c, d, a, b, x[i+10], 15, -1051523);
    b = md5_ii(b, c, d, a, x[i+ 1], 21, -2054922799);
    a = md5_ii(a, b, c, d, x[i+ 8], 6 ,  1873313359);
    d = md5_ii(d, a, b, c, x[i+15], 10, -30611744);
    c = md5_ii(c, d, a, b, x[i+ 6], 15, -1560198380);
    b = md5_ii(b, c, d, a, x[i+13], 21,  1309151649);
    a = md5_ii(a, b, c, d, x[i+ 4], 6 , -145523070);
    d = md5_ii(d, a, b, c, x[i+11], 10, -1120210379);
    c = md5_ii(c, d, a, b, x[i+ 2], 15,  718787259);
    b = md5_ii(b, c, d, a, x[i+ 9], 21, -343485551);
 
    a = safe_add(a, olda);
    b = safe_add(b, oldb);
    c = safe_add(c, oldc);
    d = safe_add(d, oldd);
  }
  return Array(a, b, c, d);
 
}
 
/*
 * These functions implement the four basic operations the algorithm uses.
 */
function md5_cmn(q, a, b, x, s, t)
{
  return safe_add(bit_rol(safe_add(safe_add(a, q), safe_add(x, t)), s),b);
}
function md5_ff(a, b, c, d, x, s, t)
{
  return md5_cmn((b & c) | ((~b) & d), a, b, x, s, t);
}
function md5_gg(a, b, c, d, x, s, t)
{
  return md5_cmn((b & d) | (c & (~d)), a, b, x, s, t);
}
function md5_hh(a, b, c, d, x, s, t)
{
  return md5_cmn(b ^ c ^ d, a, b, x, s, t);
}
function md5_ii(a, b, c, d, x, s, t)
{
  return md5_cmn(c ^ (b | (~d)), a, b, x, s, t);
}
 
/*
 * Calculate the HMAC-MD5, of a key and some data
 */
function core_hmac_md5(key, data)
{
  var bkey = str2binl(key);
  if(bkey.length > 16) bkey = core_md5(bkey, key.length * chrsz);
 
  var ipad = Array(16), opad = Array(16);
  for(var i = 0; i < 16; i++)
  {
    ipad[i] = bkey[i] ^ 0x36363636;
    opad[i] = bkey[i] ^ 0x5C5C5C5C;
  }
 
  var hash = core_md5(ipad.concat(str2binl(data)), 512 + data.length * chrsz);
  return core_md5(opad.concat(hash), 512 + 128);
}
 
/*
 * Add integers, wrapping at 2^32. This uses 16-bit operations internally
 * to work around bugs in some JS interpreters.
 */
function safe_add(x, y)
{
  var lsw = (x & 0xFFFF) + (y & 0xFFFF);
  var msw = (x >> 16) + (y >> 16) + (lsw >> 16);
  return (msw << 16) | (lsw & 0xFFFF);
}
 
/*
 * Bitwise rotate a 32-bit number to the left.
 */
function bit_rol(num, cnt)
{
  return (num << cnt) | (num >>> (32 - cnt));
}
 
/*
 * Convert a string to an array of little-endian words
 * If chrsz is ASCII, characters >255 have their hi-byte silently ignored.
 */
function str2binl(str)
{
  var bin = Array();
  var mask = (1 << chrsz) - 1;
  for(var i = 0; i < str.length * chrsz; i += chrsz)
    bin[i>>5] |= (str.charCodeAt(i / chrsz) & mask) << (i%32);
  return bin;
}
 
/*
 * Convert an array of little-endian words to a string
 */
function binl2str(bin)
{
  var str = "";
  var mask = (1 << chrsz) - 1;
  for(var i = 0; i < bin.length * 32; i += chrsz)
    str += String.fromCharCode((bin[i>>5] >>> (i % 32)) & mask);
  return str;
}
 
/*
 * Convert an array of little-endian words to a hex string.
 */
function binl2hex(binarray)
{
  var hex_tab = hexcase ? "0123456789ABCDEF" : "0123456789abcdef";
  var str = "";
  for(var i = 0; i < binarray.length * 4; i++)
  {
    str += hex_tab.charAt((binarray[i>>2] >> ((i%4)*8+4)) & 0xF) +
           hex_tab.charAt((binarray[i>>2] >> ((i%4)*8  )) & 0xF);
  }
  return str;
}
 
/*
 * Convert an array of little-endian words to a base-64 string
 */
function binl2b64(binarray)
{
  var tab = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
  var str = "";
  for(var i = 0; i < binarray.length * 4; i += 3)
  {
    var triplet = (((binarray[i   >> 2] >> 8 * ( i   %4)) & 0xFF) << 16)
                | (((binarray[i+1 >> 2] >> 8 * ((i+1)%4)) & 0xFF) << 8 )
                |  ((binarray[i+2 >> 2] >> 8 * ((i+2)%4)) & 0xFF);
    for(var j = 0; j < 4; j++)
    {
      if(i * 8 + j * 6 > binarray.length * 32) str += b64pad;
      else str += tab.charAt((triplet >> 6*(3-j)) & 0x3F);
    }
  }
  return str;
}

/*global Int32Array */
function signed_crc_table() {
	var c = 0, table = new Array(256);

	for(var n =0; n != 256; ++n){
		c = n;
		c = ((c&1) ? (-306674912 ^ (c >>> 1)) : (c >>> 1));
		c = ((c&1) ? (-306674912 ^ (c >>> 1)) : (c >>> 1));
		c = ((c&1) ? (-306674912 ^ (c >>> 1)) : (c >>> 1));
		c = ((c&1) ? (-306674912 ^ (c >>> 1)) : (c >>> 1));
		c = ((c&1) ? (-306674912 ^ (c >>> 1)) : (c >>> 1));
		c = ((c&1) ? (-306674912 ^ (c >>> 1)) : (c >>> 1));
		c = ((c&1) ? (-306674912 ^ (c >>> 1)) : (c >>> 1));
		c = ((c&1) ? (-306674912 ^ (c >>> 1)) : (c >>> 1));
		table[n] = c;
	}

	return typeof Int32Array !== 'undefined' ? new Int32Array(table) : table;
}

var T = signed_crc_table();
function crc32_bstr(bstr, seed) {
	var C = seed ^ -1, L = bstr.length - 1;
	for(var i = 0; i < L;) {
		C = (C>>>8) ^ T[(C^bstr.charCodeAt(i++))&0xFF];
		C = (C>>>8) ^ T[(C^bstr.charCodeAt(i++))&0xFF];
	}
	if(i === L) C = (C>>>8) ^ T[(C ^ bstr.charCodeAt(i))&0xFF];
	return C ^ -1;
}

function crc32_str(str, seed) {
  var C = seed ^ -1;
  for(var i = 0, L=str.length, c, d; i < L;) {
    c = str.charCodeAt(i++);
    if(c < 0x80) {
      C = (C>>>8) ^ T[(C ^ c)&0xFF];
    } else if(c < 0x800) {
      C = (C>>>8) ^ T[(C ^ (192|((c>>6)&31)))&0xFF];
      C = (C>>>8) ^ T[(C ^ (128|(c&63)))&0xFF];
    } else if(c >= 0xD800 && c < 0xE000) {
      c = (c&1023)+64; d = str.charCodeAt(i++)&1023;
      C = (C>>>8) ^ T[(C ^ (240|((c>>8)&7)))&0xFF];
      C = (C>>>8) ^ T[(C ^ (128|((c>>2)&63)))&0xFF];
      C = (C>>>8) ^ T[(C ^ (128|((d>>6)&15)|((c&3)<<4)))&0xFF];
      C = (C>>>8) ^ T[(C ^ (128|(d&63)))&0xFF];
    } else {
      C = (C>>>8) ^ T[(C ^ (224|((c>>12)&15)))&0xFF];
      C = (C>>>8) ^ T[(C ^ (128|((c>>6)&63)))&0xFF];
      C = (C>>>8) ^ T[(C ^ (128|(c&63)))&0xFF];
    }
  }
  return C ^ -1;
}

function getRoomId(roomname,password)
{
  let h = hex_md5(roomname+password);
  return h+crc32_str(h);
}

function checkRoomId(h)
{
  let m = h.substr(0,32);
  let c = h.substr(32);
  return c==crc32_str(m);
}

// 生成随机数
function getRandomString(len) {
    len = len || 32;
    var $chars = "ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678"; // 默认去掉了容易混淆的字符oOLl,9gq,Vv,Uu,I1
    var maxPos = $chars.length;
    var pwd = "";
    for (var i = 0; i < len; i++) {
      pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
    }
    return pwd;
}

// 判断是否是IE浏览器
function isIE() {
    if(!!window.ActiveXObject || "ActiveXObject" in window){
        return true;
    } else {
        return false;
    }
}
// 判断是否支持webrtc
function isSupportWebRTC() {
    if (UmVideo.isWebRtcEnable() == true) {
        return true;
    } else {
        return false;
    }
}
// 判断是否是iPhone设备中的Safari浏览器
function isSafari() {
    var u = navigator.userAgent;
    var iOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios终端
    if (iOS) {
        if (u.indexOf("Safari") > -1) {
            return true
        } else {
            return false
        }
    } else {
        return true
    }
}
// 截取location方法
function GetUrlParam(paraName) {
    var url = document.location.toString();
    var arrObj = url.split("?");
    if (arrObj.length > 1) {
        var arrPara = arrObj[arrObj.length - 1].split("&");
        var arr;
        for (var i = 0; i < arrPara.length; i++) {
            arr = arrPara[i].split("=");
            if (arr != null && arr[0] == paraName) {
                return arr[1].split('#/').join("");
            }
        }
        return "";
    } else {
        return "";
    }
}
// 做一些判断
function limitAll() {
    if (isIE()) {
        alert("请更换浏览器，建议使用Google Chrome、Firefox等浏览器！");
        // return true;
    }
    if (!isSupportWebRTC()) {
        alert("浏览器不支持本服务！请更换浏览器，建议使用Google Chrome、Firefox等浏览器！");
        // return true;
    }
    if (!isSafari()) {
        alert("请点击右上角“...”使用Safari打开！");
        return true;
    }
}
if (limitAll()) {
    return false;
}

;(function (global) {
    var moduleName = "UmRoom";
    var methods = {};
    var previous_mymodule = global[moduleName];

    // export methods
    methods.getRoomId = getRoomId;
    methods.checkRoomId = checkRoomId;


    // Support no conflict
    methods.noConflict = function() {
        global[moduleName] = previous_mymodule;
        return methods;
    }

    // - From async.js
    // AMD / RequireJS
    if (typeof define !== 'undefined' && define.amd) {
        define(moduleName, [], function () {
            return methods;
        });
    }
    // Node.js
    else if (typeof module !== 'undefined' && module.exports) {
        module.exports = methods;
    }
    // included directly via <script> tag
    else {
        global[moduleName] = methods;
    }

})(this);

    var roomName, deviceid, interval, checkInterval;
    var audio_status = true;
    var video_status = true;
    var voice_status = true;
    var joinBtn = document.querySelector('#joinRoom');
    var hungupBtn = document.querySelector("#hungup");
    var box = document.getElementById("enterBox");
    var createQrcode = document.getElementById("createURL");
    var my_name = null;
    var member_arr = [];
    var noSleep = new NoSleep();
    document.getElementById("roomInput").value = getRandomString(6); // 生成随机房间号
    document.getElementById("roomPwd").value = getRandomString(6); // 生成随机密码
    document.getElementById("defaultLink").innerHTML = '<a href="'+ window.location.href + '" target="_self" >' + window.location.href + '</a>'
    document.getElementById("help").href = "https://" + location.hostname + "/webrtc/readme.html";
    console.log("UmVideo Version:", UmVideo.version());
    UmVideo.on('userid', function (){
        console.log("my userid:", UmVideo.userid());
        if (roomName) {
            if (!my_name) {
                let local_name = localStorage.getItem("myName") ? localStorage.getItem("myName") : "";
                let str = prompt('请输入名称', local_name);
                my_name = str != "" ? str : UmVideo.userid();
                localStorage.setItem("myName", my_name);
            }
            UmVideo.joinRoom(roomName, deviceid, 
                {displayname: my_name, 
                 myid: UmVideo.userid(),
                audio_status: audio_status, 
                video_status: video_status, 
                voice_status: voice_status});
        } else {
            console.error("RoomName is empty");
        }
    });

    // 处理有人加入房间时
    UmVideo.on('hi', function(data) {
        console.log("有人加入房间: ", data);
    });

    async function hungup(e) {
        try {
            roomName="";
            UmVideo.hungup();
        } catch (e) {
            handleError(e);
        }
    };
    async function joinRoom(e,cid) {
        if (roomName && roomName==e)
        {
            return;
        }
        try {
            roomName = e
            deviceid = cid
            var wsproto = (location.protocol === 'https:') ? 'wss:' : 'ws:';
            var conn = wsproto + '//' + window.location.host + '/signal.ump';
            var obj = {video: false, audio: true};
            UmVideo.setup({
                //signalserver: "wss://localhost:4430/signal"  // or
                //signalserver: "ws://localhost:8088/signal"
                signalserver: conn,
                getusermedianow: true,
                mediaconf: obj
            });
            // 获取mediaconf里边的状态控制显示哪个按钮
            if (UmVideo.config().mediaconf.audio) {
                document.getElementById("control_audio").src = "./img/start_audio.png";
                audio_status = true;
            } else {
                audio_status = false;
                document.getElementById("control_audio").src = "./img/ban_audio.png";
            }
            if (UmVideo.config().mediaconf.video) {
                document.getElementById("control_video").src = "./img/start_video.png";
                video_status = true;
            } else {
                document.getElementById("control_video").src = "./img/ban_video.png";
                video_status = false;
            }
            // 设置定时器判断每台设备的链接状态
            checkInterval = setInterval(() => {
                var video_arr = document.getElementById("remotes").getElementsByTagName("video");
                for (var i = 0; i < video_arr.length; i++) {
                    var id = video_arr[i].id;
                    if (id.indexOf("a") != -1) {
                        var str_id = id.substring(1);
                        var status = UmVideo.peers()[str_id].connectionState;
                        console.log(2333, checkInterval, status, str_id);
                        if (status && status != "connected" && status != "connecting") {
                            UmVideo.hungup(str_id);
                            UmVideo.joinRoom(roomName, str_id, 
                                {displayname: my_name, 
                                myid: UmVideo.userid(),
                                audio_status: audio_status, 
                                video_status: video_status, 
                                voice_status: voice_status});
                        }
                    }
                }
            }, 5000);
            return
        } catch (e) {
            handleError(e);
        }
    };
    document.getElementById("calltypevi").addEventListener("click", ()=>{
        location.href = "./index.html";
    });
    joinBtn.addEventListener('click', function () {
        var roomInput = document.getElementById("roomInput").value; // 房间号
        var roomPwd = document.getElementById("roomPwd").value; // 房间密码
        if (roomInput.length == 0 && roomPwd.length == 0) {
            alert("请输入房间号和房间密码!");
            return
        } else if (roomInput.length == 0) {
            alert("请输入房间号!");
            return
        } else if (roomPwd.length == 0) {
            alert("请输入房间密码!");
            return
        } else {
            let roomInput = document.getElementById("roomInput").value;
            let roomPwd = document.getElementById("roomPwd").value;
            let roomName = getRoomId(roomInput, roomPwd);
            box.style.display = "none";
            joinRoom(roomName);
        }
    });
    // 监听remotes下的video标签
    document.getElementById("remotes").addEventListener("DOMNodeInserted", (e)=>{
        if (e.target.nodeName == "VIDEO") {
            var fa_box = document.getElementById("remotes");
            var div_box = fa_box.getElementsByTagName('div');
            var length = div_box.length;
            for (let i = 0; i < length; i++) {
                if (div_box[0]) {
                    fa_box.removeChild(div_box[0]);
                }
            }
            UmVideo.listMembers();
            UmVideo.on("memberslist", function(data) {
                member_arr = data.members;
                member_arr.forEach(val => {
                    var div_box = document.createElement("div");
                    var son_box = document.createElement("div");
                    div_box.className = "call-peo";
                    div_box.id = val.info.myid;
                    son_box.className = "idname";
                    son_box.innerHTML = val.info.displayname;
                    var new_div = document.getElementById("remotes").appendChild(div_box);
                    new_div.appendChild(son_box);
                    son_box.style.color = "#ffffff";
                    son_box.style.width = "90%";
                    son_box.style.overflow = "hidden";
                    son_box.style.textAlign = "center";
                    son_box.style.textOverflow = "ellipsis";
                    son_box.style.whiteSpace = "nowrap";
                    son_box.style.position = "absolute";
                    son_box.style.bottom = "10%";
                    son_box.style.left = "5%";
                    if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
                        div_box.style.width = "calc(50% - 2px)";
                    }
                })
            });
        }
    })
    document.getElementById("remotes").addEventListener("DOMNodeRemoved", (e)=>{
        if (e.target.nodeName == "VIDEO") {
            var re_child = document.getElementById("remotes").childNodes;
            re_child.forEach(val => {
                if (val.className == "call-peo") {
                    if ("a" + val.id == e.target.id) {
                        document.getElementById("remotes").removeChild(val);
                    }
                }
            })
        }
    })
    hungupBtn.addEventListener('click', function (e) {
        hungup(e);
        box.style.display = "block";
        if (interval) {
            clearInterval(interval)
        }
        if (checkInterval) {
            clearInterval(checkInterval);
        }
        document.getElementById("cl_big_qrcode").style.display = "none"; // 关闭视频中分享的二维码
    });
    // 生成默认二维码
    new QRCode(document.getElementById("defaultQrcode"), {
        width : 100,
        height : 100,
        text : window.location.href
    });
    // 生成二维码
    createQrcode.addEventListener("click", function (){
        document.getElementById("qrcodeBox").style.display = "block"; // 显示两个二维码
        document.getElementById("qrcodeBox").style.display = "flex"; // 显示两个二维码
        let roomInput = document.getElementById("roomInput").value; // 房间号
        let roomPwd = document.getElementById("roomPwd").value; // 房间密码
        localStorage.setItem("roomPwd", document.getElementById("roomPwd").value);
        if (roomInput.length == 0 && roomPwd.length == 0) {
            alert("请输入房间号和房间密码!");
            return;
        } else if (roomInput.length == 0) {
            alert("请输入房间号!");
            return;
        } else if (roomPwd.length == 0) {
            alert("请输入房间密码!");
            return;
        } else {
            let qrcode1,qrcode2;
            let url = window.location.href;
            let arrObj = url.split("?");
            let ban = GetUrlParam("ban");
            let pj, spe, qr2;
            if (ban != "") {
                pj = "group_call.html?h=" + getRoomId(roomInput , roomPwd) + "&ban=" + ban;
                spe = '<a href="'+ arrObj[0] + '?roomid=' + roomInput + "&ban=" + ban +'" target="_blank">'+ arrObj[0] + '?roomid=' + roomInput + "&ban=" + ban + '</a>';
                qr2 = arrObj[0] + "?roomid=" + roomInput + "&ban=" + ban;
            } else {
                if (arrObj[1]) {
                    url = arrObj[0];
                }
                pj = 'group_call.html?h=' + getRoomId(roomInput , roomPwd);
                spe = '<a href="'+ arrObj[0] + '?roomid=' + roomInput + '" target="_blank">'+ arrObj[0] + '?roomid=' + roomInput +'</a>';
                qr2 = arrObj[0] + "?roomid=" + roomInput;
            }
            document.getElementById("md5_qrcode").innerHTML = ""; //移除生成的二维码，保证下边生成新的二维码
            document.getElementById("name_pwd_qrcode").innerHTML = ""; //移除生成的二维码，保证下边生成新的二维码
            document.querySelector(".tips").style.display = "block"; //第一个二维码
            document.querySelector(".another").style.display = "block"; //第二个二维码
            document.getElementById("defaultQrcode").style.display = "none"; //隐藏默认二维码
            document.getElementById("defaultLink").style.display = "none"; // 隐藏默认连接
            // 拼接加密后的链接
            history.replaceState(null, null, pj);
            var str = '<p style="margin: 0;padding: 0;font-size: 16px;font-weight: bold;margin-bottom: 4px;">1.令牌直接加入模式</p>';
            var str1 = "【联信UMWebRTC视频会议服务】点击链接 ";
            var str2 = '<a href="'+ window.location.href +'" target="_blank">'+ window.location.href +'</a>';
            var str3 = "，在Google Chrome、Firefox及兼容的浏览器打开即时参与视频对话或会议。";
            document.getElementById("md5_tips").innerHTML = str + str1 + str2 + str3;
            // 拼接带房间密码的链接
            var strp = '<p style="margin: 0;padding: 0;font-size: 16px;font-weight: bold;margin-bottom: 4px;">2.密码加入模式</p>';
            var strp1 = spe;
            var strp2 = "，在Google Chrome、Firefox及兼容的浏览器打开，输入参会密码【" + roomPwd + "】。";
            document.getElementById("name_pwd_tips").innerHTML = strp + str1 + strp1 + strp2;
            qrcode1 = new QRCode(document.getElementById("md5_qrcode"), {
                width : 100,
                height : 100,
                text: window.location.href
            });
            qrcode2 = new QRCode(document.getElementById("name_pwd_qrcode"), {
                width : 100,
                height : 100,
                text: qr2
            });
        }
    });
    // 手机适配
    if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
        document.getElementById("title").style.fontSize = "22px";
        document.getElementById("qrcodeBox").style.flexDirection = "column";
        document.querySelector(".tips").style.float = "none";
        document.querySelector(".tips").style.width = "100%";
        document.querySelector(".tips").style.textAlign = "center";
        document.querySelector(".tips").style.marginBottom = "20px";
        document.getElementById("md5_qrcode").style.marginBottom = "20px";
        document.querySelector(".another").style.float = "none";
        document.querySelector(".another").style.width = "100%";
        document.querySelector(".another").style.textAlign = "center";
        document.querySelector(".another").style.marginBottom = "20px";
        document.getElementById("name_pwd_qrcode").style.marginBottom = "20px";
        document.getElementById("qrcodeBox").style.marginTop = "20px";
        document.querySelector(".function_ul").style.width = "300px";
        document.querySelector(".cl_big_qrcode").style.top = "-135px";
    }
    var roomid = GetUrlParam("roomid");
    var pwd = GetUrlParam("pwd");
    var h = GetUrlParam("h"); // h后边跟着的是md5加密后的
    var cid = GetUrlParam("cid"); // cid后边跟着的是设备ID，例如摄像头
    // 存在h的时候直接加入房间
    if (h) {
        try {
            if (!judgeWebRTC()) {
                throw false;
            } else if (checkRoomId(h)===false){
                alert("房间信息校验失败！");
                throw false;
            } else {
                joinRoom(h,cid);
                box.style.display = "none";
            }
        } catch (error) {
        }
        
    } else {
        var roomInput = document.getElementById("roomInput").value; // 房间号
        var roomPwd = document.getElementById("roomPwd").value; // 房间密码
        // 不存在h的时候 MD5加密(房间id + pwd)
        if (roomid.length != 0 && pwd.length != 0) {
            joinRoom(getRoomId(roomid, pwd));
            document.getElementById("roomInput").value  = roomid;
            document.getElementById("roomPwd").value = pwd;
            box.style.display = "none";
        } else if (roomid.length != 0 && pwd.length == 0) {
            document.getElementById("roomInput").value = roomid;
        } else if (roomid.length == 0 && pwd.length != 0) {
            document.getElementById("roomPwd").value = pwd;
        }
    }

    // 改变语音状态
    document.getElementById("control_audio").addEventListener("click", ()=>{
        if (audio_status) {
            UmVideo.setSelfAudio(false);  
            document.getElementById("control_audio").src = "./img/ban_audio.png";
            audio_status = false;
        } else {
            UmVideo.setSelfAudio(true);  
            document.getElementById("control_audio").src = "./img/start_audio.png";
            audio_status = true;
        }
    });
    // 改变扬声器状态
    document.getElementById("control_voice").addEventListener("click", ()=>{
        if (voice_status) {
            UmVideo.setOtherAudio(false);
            document.getElementById("control_voice").src = "./img/ban_voice.png";
            voice_status = false;
        } else {
            UmVideo.setOtherAudio(true);  
            document.getElementById("control_voice").src = "./img/start_voice.png";
            voice_status = true;
        }
    });
    // 改变视频状态
    document.getElementById("control_video").addEventListener("click", ()=>{
        if (video_status) {
            document.getElementById("control_video").src = "./img/ban_video.png";
            UmVideo.setSelfVideo(false);  
            video_status = false;
        } else {
            if (UmVideo.config().mediaconf.video == false) {
                alert("通话过程中不允许打开摄像头！");
                return;
            } else {
                document.getElementById("control_video").src = "./img/start_video.png";
                UmVideo.setSelfVideo(true);  
                video_status = true;
            }
        }
    });
    // 直接关闭网页或者浏览器触发挂断方法
    window.onbeforeunload = function () {
        hungup();
    };
    window.onunload = function () {
        hungup();
    };
    window.onpagehide = function () {
        hungup();
    };

    // 点击视频中二维码生成分享二维码
    var click_qrcode1;
    var qrcode_btn = document.getElementById("qrcode");
    var click_qrcode = document.getElementById("click_qrcode");
    var cl_big_qrcode = document.getElementById("cl_big_qrcode"); // 获取视频中点击分享的二维码盒子
    var url = window.location.href;
    qrcode_btn.addEventListener("click", ()=>{
        click_qrcode.innerHTML = ""; //移除生成的二维码，保证下边生成新的二维码
        if (cl_big_qrcode.style.display == "none") {
            cl_big_qrcode.style.display = "block";
        } else if (cl_big_qrcode.style.display == "block") {
            cl_big_qrcode.style.display = "none";
        }
        let loca_url = window.location.href;
        let new_url = loca_url.split("?");
        let ban = GetUrlParam("ban");
        var an_url;
        if (ban != "") {
            an_url = new_url[0] + "?h=" + roomName + "&ban=" + ban;
        } else {
            an_url = new_url[0] + "?h=" +roomName;
        }
        click_qrcode1 = new QRCode(click_qrcode, {
            width: 100,
            height: 100,
            text: an_url
        });
        if ((navigator.userAgent.match(/(phone|pad|pod|iPhone|iPod|ios|iPad|Android|Mobile|BlackBerry|IEMobile|MQQBrowser|JUC|Fennec|wOSBrowser|BrowserNG|WebOS|Symbian|Windows Phone)/i))) {
            document.querySelector(".click_qrcode").getElementsByTagName("img")[0].style.setProperty("width","100px","important");
        }
    });

    // 监听按钮，触发NoSleep
    document.getElementById("btn").addEventListener("click", function (e) {
        noSleep.enable();
        e.target.style.display = "none";
    })
    document.getElementById("btn").click();
    // Demo
    // UmVideo.setSelfVideo(false);    关闭或开启视频
    // UmVideo.setSelfAudio(false);    关闭或开启音频
    // UmVideo.isWebRtcEnable();       是否支持WebRtc